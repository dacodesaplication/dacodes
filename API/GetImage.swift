//
//  GetImage.swift
//  moviesApp
//
//  Created by Aaron on 23/11/20.
//

import Foundation

struct GetImage : Codable {
    var images : GetImageStruct
}

struct GetImageStruct : Codable{
    var base_url : String
    var secure_base_url : String
    var poster_sizes : [String]
}
