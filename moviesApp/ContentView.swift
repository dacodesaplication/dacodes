//
//  ContentView.swift
//  moviesApp
//
//  Created by Aaron on 23/11/20.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var networkingManager = NetWorkingManager()
    
    var body: some View {
        NavigationView{
            
            ScrollView(.vertical, showsIndicators: false, content: {
                LazyVGrid(columns: [
                    GridItem(.fixed(200)),
                    GridItem(.fixed(200))
                ],content: {
                    ForEach(self.networkingManager.getList.results, id: \.id) { i in
                        HStack{
                            NavigationLink(
                                destination: DetallePeliculas(id: i.id)){
                                ZStack(alignment:.bottomTrailing){
                                    imageView(imageUrl: networkingManager.urlImagenBase + i.poster_path!)
                                    VStack (alignment:.leading){
                                        Text(i.title)
                                            .foregroundColor(.white)
                                            .font(.subheadline)
                                        HStack{
                                            
                                            Text(i.release_date)
                                                .foregroundColor(.white)
                                                .font(.subheadline)
                                            Spacer()
                                            Image(systemName: "star")
                                                .foregroundColor(.yellow)
                                            Text(String(i.vote_average))
                                                .font(.subheadline)
                                                .foregroundColor(.white)
                                            
                                        }//FIN HSTACK
                                        
                                    }//FIN ZSTACK
                                    .background(Color.black)
                                    .opacity(0.7)
                                    
                                }//FIN HSTACK
                            }//FIN VSTACK
                        }
                    }//FIN FOREACH
                })
            })
            
            
            .navigationBarTitle(Text("Peliculas"),displayMode: .inline)
        }//FIN NAVIGATION
        
    }
}


struct imageView : View{
    @ObservedObject var imageLoader : ImageLoader
    
    init(imageUrl:String){
        imageLoader = ImageLoader.init(imageurl: imageUrl)
    }
    var body: some View{
        Image(uiImage: ((imageLoader.data.count == 0) ? UIImage(systemName: "photo"): UIImage(data: imageLoader.data))!)
            .resizable()
            .renderingMode(.original)
//            .aspectRatio(contentMode: .fill)
            .frame(width:200,height: 300)
            .clipped()
            .cornerRadius(10)
            .shadow(radius: 10)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
