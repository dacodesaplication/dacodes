//
//  GetList.swift
//  moviesApp
//
//  Created by Aaron on 23/11/20.
//

import Foundation

struct GetList : Decodable {
    var results : [GetListStruct]
}

struct GetListStruct : Decodable {
    var id : Int
    var vote_average : Float
    var poster_path : String?
    var title : String
    var release_date : String
}


