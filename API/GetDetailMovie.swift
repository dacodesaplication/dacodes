//
//  GetDetailMovie.swift
//  moviesApp
//
//  Created by Aaron on 23/11/20.
//

import Foundation
import SwiftUI
import Combine

struct GetDetailMovie : Decodable{
    var backdrop_path : String
    var overview : String
    var original_title : String
    var title : String
    var genres : [Genres]
    var runtime : Int
    var vote_average: Float
    var release_date : String
}

struct Genres : Decodable {
    var id : Int
    var name: String
}

//GET - Detalle de Pelicula Seleccionada

class Api : ObservableObject {
    @ObservedObject var GlobalUrl = GlobalConfiguration()
    var didChange = PassthroughSubject<Api,Never>()
    
    @Published var getDetailM = GetDetailMovie(backdrop_path: "",
                                               overview: "",
                                               original_title: "",
                                               title: "",
                                               genres: [Genres](),
                                               runtime: 0,
                                               vote_average: 0.0,
                                               release_date: ""){
        didSet{
            didChange.send(self)
        }
    }
    
    init(id: Int){
        guard let url = URL(string: GlobalUrl.URlMovieDetail + "\(id)?api_key=\(GlobalUrl.ApiKey)" ) else { return }
        print(url)
        URLSession.shared.dataTask(with: url) { (results,response, _) in
            do{
                guard let data = results else {return}
                let moviesDetail = try JSONDecoder().decode(GetDetailMovie.self, from: data)
                DispatchQueue.main.async {
                    self.getDetailM = moviesDetail
                }
            }catch let error as NSError{
                print("Ocurrio un error en la API",error.localizedDescription)
            }
        }.resume()
    }
}
