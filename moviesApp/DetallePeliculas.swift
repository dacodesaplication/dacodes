//
//  DetallePeliculas.swift
//  moviesApp
//
//  Created by Aaron on 23/11/20.
//

import SwiftUI


struct DetallePeliculas: View {
    var id : Int = 0
    
    @ObservedObject var api : Api
    @ObservedObject var networkingManager = NetWorkingManager()
    
    init(id:Int){
        api = Api.init(id: id)
        print(id)
    }
    
    var body: some View {
        let data = api.getDetailM
        ScrollView{
            VStack (spacing:0){
                VStack(spacing:0){
                    HStack{
                        Spacer()
                    ImageView(imageUrl: networkingManager.urlImagenBase + data.backdrop_path)
                        Spacer()
                    }
                }
                VStack(alignment:.leading, spacing:2){
                    
                    Group{
                        Text(data.title)
                            .font(.title)
                    }
                    
                    Group{
                        Text("Duración:")
                            .font(.subheadline).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        Text("\(data.runtime) min")
                    }
                    Group{
                        Text("Fecha de Estreno")
                            .font(.subheadline).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        Text(data.release_date)
                    }
                    Group{
                        Text("Calificación:")
                            .font(.subheadline).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        Text(String(data.vote_average))
                    }
                    Group{
                        Text("Géneros:")
                            .font(.subheadline).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        HStack{
                            ForEach(data.genres,id: \.id){ i in
                                Text(i.name)
                            }
                        }
                    }
                    //Fin Group
                    Group{
                        Text("Descripción")
                            .font(.subheadline).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                        Text(data.overview)
                    }
                }//FIN VSTACK GROUP
                .frame(width: 400, height: 400, alignment: .leading)
            }
            Spacer()
            //Stack Principal
        }//FinScroll
    }//FIN VIEW
}

struct ImageView : View {
    @ObservedObject var imageLoader : ImageLoader
    
    init(imageUrl:String){
        imageLoader = ImageLoader.init(imageurl: imageUrl)
    }
    
    var body: some View {
        
        Image(uiImage: ((imageLoader.data.count == 0) ? UIImage(systemName: "photo"): UIImage(data: imageLoader.data))!)
    }
}

struct DetallePeliculas_Previews: PreviewProvider {
    static var previews: some View {
        DetallePeliculas(id: 0)
        
        
    }
}
