//
//  NetworkingManager.swift
//  moviesApp
//
//  Created by Aaron on 23/11/20.
//

import Foundation
import SwiftUI
import Combine

class NetWorkingManager : ObservableObject {
    
    var didChange = PassthroughSubject<NetWorkingManager,Never>()
    @ObservedObject var globalurl = GlobalConfiguration()
    @Published var urlImagenBase : String = ""
    
    @Published var getList = GetList(results : []){
        didSet{
            didChange.send(self)
        }
    }
        
    //Constructor
    init() {
        
        //GET DE PELICULAS
        guard let url = URL(string: globalurl.UrlMovies + globalurl.ApiKey ) else { return }
        URLSession.shared.dataTask(with: url) { (results,response, _) in
            if let response = response{
                print(response)
            }
            
            do{
                guard let data = results else {return}
                let movies = try JSONDecoder().decode(GetList.self, from: data)
                DispatchQueue.main.async {
                    self.getList = movies
                }
            }catch let error as NSError{
                print("Ocurrio un error en la API",error.localizedDescription)
            }
        }.resume()
        
        //GET URL BASE DE IMAGEN
        loadData()
    }
    
    
    //PRUEBA
    func loadData() {
        guard let url = URL(string: globalurl.UrlConfBaseImagen + globalurl.ApiKey) else {
            return
        }
        URLSession.shared.dataTask(with: url) { images, response, error in
            
            do{
                guard let data = images else {return}
                let imagen = try JSONDecoder().decode(GetImage.self, from: data)
                print(imagen.images.secure_base_url + imagen.images.poster_sizes[4])
                self.urlImagenBase = imagen.images.secure_base_url + imagen.images.poster_sizes[4]
            }catch let error as NSError{
                print("Ocurrio un error en la API",error.localizedDescription)
            }
        }.resume()
    }
    
}
