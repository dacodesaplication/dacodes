//
//  ImageLoader.swift
//  moviesApp
//
//  Created by Aaron on 23/11/20.
//

import Foundation
import SwiftUI
import Combine

class ImageLoader: ObservableObject {
    var didChange = PassthroughSubject<Data,Never>()
    
    @Published var data = Data(){
        didSet{
            didChange.send(data)
        }
    }
    
    init(imageurl:String){
        guard let url = URL(string: imageurl) else { return }
        URLSession.shared.dataTask(with: url){ (data,_,_) in
            guard let data = data else { return }
            DispatchQueue.main.async {
                self.data = data
            }
        }.resume()
    }
}
