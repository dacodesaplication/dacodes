//
//  GlobalConfiguration.swift
//  moviesApp
//
//  Created by Aaron on 23/11/20.
//

import Foundation
import SwiftUI
import Combine

class GlobalConfiguration: ObservableObject {
    @Published var ApiKey = "634b49e294bd1ff87914e7b9d014daed"
    @Published var UrlMovies = "https://api.themoviedb.org/3/movie/now_playing?api_key="
    @Published var UrlConfBaseImagen = "https://api.themoviedb.org/3/configuration?api_key="
    @Published var URlMovieDetail = "https://api.themoviedb.org/3/movie/"
    
}
